<?php

namespace Pafi2010\Object\Facades;

use Illuminate\Support\Facades\Facade;

class Object extends Facade
{
    /**
     * Получить зарегистрированное имя компонента.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'object'; }

}