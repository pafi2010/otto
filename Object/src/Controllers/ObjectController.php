<?php

namespace Pafi2010\Object\Controllers;

use App\Examples\Pets;
use Illuminate\Routing\Controller as BaseController;
use Pafi2010\Object\Exeptions\ObjectsMakeException;

class ObjectController extends BaseController
{
    /**
     * Создание класса из конфига
     *
     * @param $name
     * @return null
     * @throws ObjectsMakeException
     */
    public function make($name)
    {

        if (!array_key_exists($name,config('object.values'))) {
            throw new ObjectsMakeException();
        }

        $config = config('object.values')[$name];

        if (!is_subclass_of($config['class'], Pets::class, true)) {
            throw new ObjectsMakeException();
        }

        $instance = new $config['class']($config['age']);

        return $instance;
    }
}