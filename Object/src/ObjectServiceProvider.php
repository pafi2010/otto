<?php

/**
 * Created by PhpStorm.
 * User: Павел
 * Date: 02.11.2017
 * Time: 1:33
 */

namespace Pafi2010\Object;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

use Pafi2010\Object\Controllers\ObjectController;
use Pafi2010\Object\Facades\Object;

class ObjectServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('object', function () {
            return new ObjectController();
        });
    }

    public function boot()
    {
        //Add facade global alias
        AliasLoader::getInstance()->alias('Object', Object::class);

        $this->publishes([
            __DIR__.'/Config/object.php' => config_path('object.php'),
            __DIR__.'/Examples' => app_path('Examples'),
        ]);
    }
}