<?php

return [

    'values' => [
        'Dog' => ['class' => App\Examples\Dogs::class, 'age' => 7],
        'Cat' => ['class' => App\Examples\Cats::class, 'age' => 3],
    ]

];