<?php

namespace App\Examples;


abstract class Pets
{

    protected $age;

    /**
     * Pets constructor.
     * @param $age
     */
    public function __construct($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function age()
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function _class()
    {
        return get_class($this);
    }

}